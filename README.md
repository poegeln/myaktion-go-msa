# myaktion-go-msa

An examplary application for the microservice-architecture provided under https://gitlab.reutlingen-university.de/poegel/microservice-architecture.

## Getting started

To make use of this example-application, the [base microservice-architecture](https://gitlab.reutlingen-university.de/poegel/microservice-architecture) is required. Make sure it is deployed correctly before deploying the example provided in this repository.

The code used for this example uses [myaktion-go-2023](https://github.com/turngeek/myaktion-go-2023) as it's basis and only has been slighty adjusted to make use of the authentication service provided by the base microservices-architecture.

## Deploying the application

After deploying the [base microservice-architecture](https://gitlab.reutlingen-university.de/poegel/microservice-architecture), this application can be simply deployed in the kubernetes cluster by applying all files found in the configuration directory.  
This can be achieved using following command:
```
kubectl apply -f {filename}
```
Make sure to deploy them in the correct order of: Namespace > PersistentVolume > Deployment > Service > IngressRoute

## Testing the API

To try out the application you can execute the following examplary curl-commands.  

Should the architecture not be deployed locally, change the localhost address to the actual IP-address. Every `{JWT}` needs to be replaced with a valid JWT received from the Auth-Service.

### Register first user (first registered user is the administrator)
```
curl -i --location 'localhost/user' --form 'user="user1"' --form 'password="password1"'
```

### Login with existing user
```
curl -i --location 'localhost/login' --form 'user="user1"' --form 'password="password1"'
```

### Register second user (needs admin access-token)
```
curl -i --location 'localhost/user' --header 'Authorization: Bearer {JWT}' --form 'user="user2"' --form 'password="password2"'
```

### Healthcheck for MyAktionGo
```
curl --location 'localhost/health'
```

### Create Campaign
```
curl -i --location 'localhost/campaign' --header 'Content-Type: application/json' --header 'Authorization: Bearer {JWT}' --data '{
    "name": "Covid",
    "organizerName": "Martin",
    "donationMinimum": 2,
    "targetAmount": 100,
    "account": {
        "name": "Martin",
        "bankName": "DKB",
        "number": "123456"
    }
}'
```

### Get Campaigns of user
```
curl -i --location '172.26.216.220/campaigns' --header 'Authorization: Bearer {JWT}'
```

### Update Campaign
```
curl -i --location --request PUT 'localhost/campaigns/1' --header 'Content-Type: application/json' --header 'Authorization: Bearer {JWT}' --data '{
    "name": "Covid Reloaded",
    "organizerName": "Martin",
    "donationMinimum": 2,
    "targetAmount": 200,
    "account": {
        "name": "Martin",
        "bankName": "DKB",
        "number": "123456"
    }
}'
```

### Delete Campaign
```
curl -i --location --request DELETE 'localhost/campaigns/1' --header 'Authorization: Bearer {JWT}'
```

### Add Donation
```
curl -i --location 'localhost/campaigns/1/donation' --header 'Content-Type: application/json' --data '{
    "Amount": 20,
    "donorName": "Martin",
    "receiptRequested": true,
    "status": "IN_PROCESS",
    "account": {
        "name": "Martin",
        "bankName": "DKB",
        "number": "123456"
    }
}'
```
