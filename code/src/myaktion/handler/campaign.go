package handler

import (
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/turngeek/myaktion-go-2023/src/myaktion/model"
	"github.com/turngeek/myaktion-go-2023/src/myaktion/service"
)

func getCampaign(r *http.Request) (*model.Campaign, error) {
	var campaign model.Campaign
	err := json.NewDecoder(r.Body).Decode(&campaign)
	if err != nil {
		log.Errorf("Can't decode request body to campaign struct: %v", err)
		return nil, err
	}
	return &campaign, nil
}

func CreateCampaign(w http.ResponseWriter, r *http.Request) {
	campaign, err := getCampaign(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	campaign.OrganizerId = r.Header.Get("Userid")
	if err := service.CreateCampaign(campaign); err != nil {
		log.Errorf("Error calling service CreateCampaign: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, campaign)
}

func GetCampaigns(w http.ResponseWriter, r *http.Request) {
	campaigns, err := service.GetCampaigns()
	if err != nil {
		log.Errorf("Error calling service GetCampaigns: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	userid := r.Header.Get("Userid")
	var campaignsOfUser []model.Campaign
	for _, campaign := range campaigns {
		if campaign.OrganizerId == userid {
			campaignsOfUser = append(campaignsOfUser, campaign)
		}
	}
	sendJson(w, campaignsOfUser)
}

func GetCampaign(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	campaign, err := service.GetCampaign(id)
	if err != nil {
		log.Errorf("Failure retrieving campaign with ID %v: %v", id, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if campaign == nil {
		http.Error(w, "404 campaign not found", http.StatusNotFound)
		return
	}
	if campaign.OrganizerId != r.Header.Get("Userid") {
		http.Error(w, "403 campaign belongs to a different user", http.StatusForbidden)
		return
	}
	sendJson(w, campaign)
}

func UpdateCampaign(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	campaign, err := getCampaign(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	existingCampaign, err := service.GetCampaign(id)
	if err != nil {
		log.Errorf("Failure retrieving campaign with ID %v: %v", id, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if existingCampaign == nil {
		http.Error(w, "404 campaign not found", http.StatusNotFound)
		return
	}
	if existingCampaign.OrganizerId != r.Header.Get("Userid") {
		http.Error(w, "403 campaign belongs to a different user", http.StatusForbidden)
		return
	}
	campaign, err = service.UpdateCampaign(id, campaign)
	if err != nil {
		log.Errorf("Failure updating campaign with ID %v: %v", id, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if campaign == nil {
		http.Error(w, "404 campaign not found", http.StatusNotFound)
		return
	}
	sendJson(w, campaign)
}

func DeleteCampaign(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	existingCampaign, err := service.GetCampaign(id)
	if err != nil {
		log.Errorf("Failure retrieving campaign with ID %v: %v", id, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if existingCampaign == nil {
		http.Error(w, "404 campaign not found", http.StatusNotFound)
		return
	}
	if existingCampaign.OrganizerId != r.Header.Get("Userid") {
		http.Error(w, "403 campaign belongs to a different user", http.StatusForbidden)
		return
	}
	campaign, err := service.DeleteCampaign(id)
	if err != nil {
		log.Errorf("Failure deleting campaign with ID %v: %v", id, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if campaign == nil {
		http.Error(w, "404 campaign not found", http.StatusNotFound)
		return
	}
	sendJson(w, result{Success: "OK"})
}
